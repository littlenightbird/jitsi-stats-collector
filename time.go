package main

import (
	"fmt"
	"log"
	"strconv"
	"time"
)

type Timer interface {
	SetTime()
}

type Timestamps struct {
	TodayBeginningDate int64
	WeekBeginningDate  int64
	MonthBeginningDate int64
	YearBeginningDate  int64
}

func (t *Timestamps) SetTime() {
	t.TodayBeginningDate = completeUnixTimestamps(int(stringToUnixTimeStamp(timeToString(todayStarting()))))
	t.WeekBeginningDate = completeUnixTimestamps(int(stringToUnixTimeStamp(timeToString(weekStarting()))))
	t.MonthBeginningDate = completeUnixTimestamps(int(stringToUnixTimeStamp(timeToString(monthStarting()))))
	t.YearBeginningDate = completeUnixTimestamps(int(stringToUnixTimeStamp(timeToString(yearStarting()))))
}

func todayStarting() time.Time {
	return time.Now()
}

func weekStarting() time.Time {
	var weekStartingDate time.Time

	for i := 0; i <= 7; i++ {
		dateTime := time.Now().AddDate(0, 0, -i)
		day := int(dateTime.Weekday())
		if day == 1 {
			weekStartingDate = dateTime
			return weekStartingDate
		}
	}
	return weekStartingDate
}

func monthStarting() time.Time {
	var monthStartingDate time.Time

	for i := 0; i <= 31; i++ {
		currentMonth := time.Now().Format("01")
		dateTime := time.Now().AddDate(0, 0, -i)
		month := dateTime.Format("01")
		if month != currentMonth {
			i--
			dateTime := time.Now().AddDate(0, 0, -i)
			monthStartingDate = dateTime
			return monthStartingDate
		}
	}
	return monthStartingDate
}

func yearStarting() time.Time {
	var yearStartingDate time.Time

	for i := 0; i <= 365; i++ {
		currentYear := time.Now().Format("2006")
		dateTime := time.Now().AddDate(0, 0, -i)
		year := dateTime.Format("2006")
		if year != currentYear {
			i--
			dateTime := time.Now().AddDate(0, 0, -i)
			yearStartingDate = dateTime
			return yearStartingDate
		}
	}
	return yearStartingDate
}

func completeUnixTimestamps(t int) int64 {
	stringEnd := "000000000"
	s := fmt.Sprintf("%d%s", t, stringEnd)
	t, err := strconv.Atoi(s)
	l.CustomError(err)
	return int64(t)
}

func timeToString(t time.Time) string {
	s := t.Format("2006-01-02")
	//	s = fmt.Sprintf("%sT00:00:00Z", s)
	s = fmt.Sprintf("%s 00:00:00", s)
	return s
}

func stringToUnixTimeStamp(s string) int64 {
	layout := "2006-01-02 15:04:05"
	unixTimeStamp, err := time.Parse(layout, s)
	if err != nil {
		log.Panic()
		return unixTimeStamp.Unix()
	}
	return unixTimeStamp.Unix()
}
