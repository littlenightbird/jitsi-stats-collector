package main

import (
	"os"

	"github.com/sirupsen/logrus"
)

type Logger interface {
	NewStandardOutput()
	CustomInfo(msg string)
	CustomError(err error)
}

type Log struct {
	CustomLog     *logrus.Logger
	CustomLogFile *logrus.Logger
}

func (c *Log) NewStandardOutput() {
	c.CustomLog = logrus.New()
	c.CustomLogFile = logrus.New()

	customFormatter := new(logrus.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 - 15:04:05"
	customFormatter.FullTimestamp = true

	c.CustomLog.SetFormatter(customFormatter)
	c.CustomLogFile.SetFormatter(customFormatter)
}

func (c *Log) CustomError(err error) {
	if err != nil {
		if e.Parameters.Log {
			f, err := os.OpenFile(e.Parameters.LogPath, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0640)
			l.CustomError(err)
			c.CustomLogFile.Error(err)
			c.CustomLogFile.SetOutput(f)
		}
		{
			c.CustomLog.Error(err)
		}
	}
}

func (c *Log) CustomInfo(msg string) {
	if e.Parameters.Log {
		c.CustomLogFile.Info(msg)
		f, err := os.OpenFile(e.Parameters.LogPath, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0640)
		l.CustomError(err)
		c.CustomLogFile.SetOutput(f)
	}
	{
		c.CustomLog.Info(msg)
	}
}

func (c *Log) CustomWarn(msg string) {
	if e.Parameters.Log {
		f, err := os.OpenFile(e.Parameters.LogPath, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0640)
		l.CustomError(err)
		c.CustomLogFile.Warning(msg)
		c.CustomLogFile.SetOutput(f)
	}
	{
		c.CustomLog.Warning(msg)
	}
}
