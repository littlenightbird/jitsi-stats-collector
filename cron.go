package main

import (
	"fmt"
	"os"
	"time"
)

type Cron interface {
	MainRoutine(e Environnement, c Connection, l Log, fOrigin string)
	TimeRoutine(e Environnement, c Connection, l Log)
}

func MainRoutine(e Environnement, c Connection, l Log, fOrigin string) {
	referenceConferenceField := fmt.Sprintf("reference_%s", fOrigin)
	statusConferenceField := fmt.Sprintf("status_%s", fOrigin)
	finalConferenceField := fmt.Sprintf("final_%s", fOrigin)
	ts := time.Now()

	// Data processing for conferences
	for _, n := range e.Target.Nodes {
		// Get Stats from original measurement
		q := BuildQuery(c.Bucket, e.Target.OriginMeasurement, fOrigin, n)
		_, intOriginalValue, _, err := c.GetQuery(q)
		l.CustomError(err)
		msg := fmt.Sprintf("[1] Get originals values into '%s' for %s: %d", fOrigin, n, intOriginalValue)
		l.CustomInfo(msg)

		// Post the original value in status field
		f := fmt.Sprintf("%s_%s", n, statusConferenceField)
		err = c.PostQuery(e.Target.DestinationMeasurement, n, f, intOriginalValue, ts)
		l.CustomError(err)
		msg = fmt.Sprintf("[2] Post originals values into '%s' fields for %s: %d", f, n, intOriginalValue)
		l.CustomInfo(msg)

		// Get stats from status field
		f = fmt.Sprintf("%s_%s", n, statusConferenceField)
		q = BuildQuery(c.Bucket, e.Target.DestinationMeasurement, f, n)
		_, intStatusValue, _, err := c.GetQuery(q)
		l.CustomError(err)
		msg = fmt.Sprintf("[3] Gets status values into '%s' fields for %s: %d", f, n, intStatusValue)
		l.CustomInfo(msg)

		// Get stats from reference field
		f = fmt.Sprintf("%s_%s", n, referenceConferenceField)
		q = BuildQuery(c.Bucket, e.Target.DestinationMeasurement, f, n)
		_, intReferenceValue, _, err := c.GetQuery(q)
		l.CustomError(err)
		msg = fmt.Sprintf("[4] Gets references values into '%s' fields for %s: %d", f, n, intReferenceValue)
		l.CustomInfo(msg)

		// Commit status value in reference field if once of instances has been restarted
		if intStatusValue < intReferenceValue {
			f := fmt.Sprintf("%s_%s", n, referenceConferenceField)
			err = c.PostQuery(e.Target.DestinationMeasurement, n, f, intStatusValue, ts)
			l.CustomError(err)
			msg = fmt.Sprintf("[*] (Reference value less than vatus value) Update of references into '%s' after detection of anomalies in the values for %s: %d", f, n, intStatusValue)
			l.CustomWarn(msg)
		} else {
			// Get current value from final field
			f = fmt.Sprintf("%s_%s", n, finalConferenceField)
			q = BuildQuery(c.Bucket, e.Target.DestinationMeasurement, f, n)
			_, intFinal, _, err := c.GetQuery(q)
			l.CustomError(err)
			msg = fmt.Sprintf("[5] Gets last final value into '%s' fields for %s: %d", f, n, intFinal)
			l.CustomInfo(msg)

			// Commit the new value to final field
			finalSum := intFinal + (intStatusValue - intReferenceValue)
			f = fmt.Sprintf("%s_%s", n, finalConferenceField)
			err = c.PostQuery(e.Target.DestinationMeasurement, n, f, finalSum, ts)
			l.CustomError(err)
			msg = fmt.Sprintf("[6] Update final value into '%s' fields for %s: %d", f, n, finalSum)
			l.CustomInfo(msg)

			// Commit new reference value from status field
			f = fmt.Sprintf("%s_%s", n, referenceConferenceField)
			err = c.PostQuery(e.Target.DestinationMeasurement, n, f, intStatusValue, ts)
			l.CustomError(err)
			msg = fmt.Sprintf("[7] Update final value into '%s' fields for %s: %d", f, n, intReferenceValue)
			l.CustomInfo(msg)
		}
	}
}

func InitRoutine(e Environnement, c Connection, l Log, fieldOrigin string, days *int) {
	referenceConferenceField := fmt.Sprintf("reference_%s", fieldOrigin)
	statusConferenceField := fmt.Sprintf("status_%s", fieldOrigin)
	finalConferenceField := fmt.Sprintf("final_%s", fieldOrigin)
	refFinal := 0
	// Data processing for conferences
	for _, n := range e.Target.Nodes {
		zeroPresence := false
		firstCommit := false
		firstValue := false

		startingRange := *days
		endindRange := *days - 1

		for endindRange >= 0 {
			initTimeStamps := time.Now().AddDate(0, 0, -(startingRange + 1))

			q := InitQuery(c.Bucket, e.Target.OriginMeasurement, fieldOrigin, n, startingRange, endindRange)
			_, intOriginalValue, queryTime, err := c.GetQuery(q)
			l.CustomError(err)
			msg := fmt.Sprintf("[1] Get originals values into '%s' for %s: value=%d, time=%v, daysAgo=%d", fieldOrigin, n, intOriginalValue, queryTime, endindRange)
			l.CustomInfo(msg)

			// Detect first
			if !firstValue && zeroPresence && firstCommit && intOriginalValue != 0 {
				f := fmt.Sprintf("%s_%s", n, finalConferenceField)
				err = c.PostQuery(e.Target.DestinationMeasurement, n, f, 0, initTimeStamps)
				l.CustomError(err)
				msg = fmt.Sprintf("[*] (Detect empty field) Init first final value into '%s' fields for %s: %d", f, n, 0)
				l.CustomWarn(msg)
				firstValue = true

				f = fmt.Sprintf("%s_%s", n, finalConferenceField)
				err = c.PostQuery(e.Target.DestinationMeasurement, n, f, intOriginalValue, queryTime)
				l.CustomError(err)
				msg = fmt.Sprintf("[*] (Detect first value) Update final value into '%s' fields for %s: %d", f, n, intOriginalValue)
				l.CustomWarn(msg)
				firstValue = true
				continue
			} else if zeroPresence && intOriginalValue == 0 {
				firstCommit = true
			} else if intOriginalValue == 0 {
				zeroPresence = true
			} else {
				firstCommit = false
			}

			// Commit directly the first value into final field

			// Commit new status value from original value field
			f := fmt.Sprintf("%s_%s", n, statusConferenceField)
			err = c.PostQuery(e.Target.DestinationMeasurement, n, f, intOriginalValue, queryTime)
			l.CustomError(err)
			msg = fmt.Sprintf("[2] Post status value into '%s' field for %s: %d", f, n, intOriginalValue)
			l.CustomInfo(msg)

			// Get stats from status field
			f = fmt.Sprintf("%s_%s", n, statusConferenceField)
			q = InitQuery(c.Bucket, e.Target.DestinationMeasurement, f, n, startingRange+5, endindRange)
			_, intStatusValue, _, err := c.GetQuery(q)
			l.CustomError(err)
			msg = fmt.Sprintf("[3] Gets status values into '%s' fields for %s: %d", f, n, intStatusValue)
			l.CustomInfo(msg)

			// Get stats from reference field
			f = fmt.Sprintf("%s_%s", n, referenceConferenceField)
			q = InitQuery(c.Bucket, e.Target.DestinationMeasurement, f, n, startingRange+5, endindRange)
			_, intReferenceValue, _, err := c.GetQuery(q)
			l.CustomError(err)
			msg = fmt.Sprintf("[4] Gets reference values into '%s' fields for %s: %d", f, n, intReferenceValue)
			l.CustomInfo(msg)

			// Get current value from final field
			f = fmt.Sprintf("%s_%s", n, finalConferenceField)
			q = InitQuery(c.Bucket, e.Target.DestinationMeasurement, f, n, startingRange+5, endindRange)
			_, intFinal, _, err := c.GetQuery(q)
			l.CustomError(err)
			msg = fmt.Sprintf("[5] Gets last final value into '%s' fields for %s: %d", f, n, intFinal)
			l.CustomInfo(msg)

			// Commit original value in reference field if once of instances has been restarted
			// Or commit status value in reference field if status is inferior than reference
			// Else continue the normal operations
			if firstValue && intOriginalValue == 0 || !firstValue {
				its := time.Now().AddDate(0, 0, -(startingRange))
				msg = fmt.Sprintf("[*] (Original value equal 0) Jump to next process after detection of anomalies in the values for %s: %d", n, intOriginalValue)
				l.CustomWarn(msg)

				if intFinal != 0 {
					f = fmt.Sprintf("%s_%s", n, finalConferenceField)
					err = c.PostQuery(e.Target.DestinationMeasurement, n, f, intFinal, its)
					l.CustomError(err)
					msg = fmt.Sprintf("[*] Update final value into '%s' fields for %s: %d", f, n, intFinal)
					l.CustomWarn(msg)
				}

				startingRange--
				endindRange--
				continue
			} else if firstValue && intReferenceValue == 0 {
				if intOriginalValue != 0 && intStatusValue != 0 {
					f := fmt.Sprintf("%s_%s", n, referenceConferenceField)
					err = c.PostQuery(e.Target.DestinationMeasurement, n, f, intStatusValue, queryTime)
					l.CustomError(err)
					msg = fmt.Sprintf("[*] (Reference value equal 0) Update of references into '%s' after detection of anomalies in the values for %s: %d", f, n, intStatusValue)
					l.CustomWarn(msg)
				}

				if intFinal != 0 {
					f = fmt.Sprintf("%s_%s", n, finalConferenceField)
					err = c.PostQuery(e.Target.DestinationMeasurement, n, f, intFinal, queryTime)
					l.CustomError(err)
					msg = fmt.Sprintf("[*] Update final value into '%s' fields for %s: %d", f, n, intFinal)
					l.CustomWarn(msg)
				}
			} else if firstValue && intStatusValue < intReferenceValue {
				f := fmt.Sprintf("%s_%s", n, referenceConferenceField)
				err = c.PostQuery(e.Target.DestinationMeasurement, n, f, intStatusValue, queryTime)
				l.CustomError(err)
				msg = fmt.Sprintf("[*] (Reference value greatter than status value) Update of references into '%s' after detection of anomalies in the values for %s: %d", f, n, intStatusValue)
				l.CustomWarn(msg)

				if intFinal != 0 && refFinal >= intFinal {
					f = fmt.Sprintf("%s_%s", n, finalConferenceField)
					err = c.PostQuery(e.Target.DestinationMeasurement, n, f, intFinal, queryTime)
					l.CustomError(err)
					msg = fmt.Sprintf("[*] Update final value into '%s' fields for %s: %d", f, n, intFinal)
					l.CustomWarn(msg)
				}
			} else {
				// Commit the new value to final field
				finalSum := intFinal + (intStatusValue - intReferenceValue)
				f = fmt.Sprintf("%s_%s", n, finalConferenceField)
				err = c.PostQuery(e.Target.DestinationMeasurement, n, f, finalSum, queryTime)
				l.CustomError(err)
				msg = fmt.Sprintf("[6] Update final value into '%s' fields for %s: {%d + (%d - %d) = %d}, value=%d", f, n, intFinal, intStatusValue, intReferenceValue, finalSum, finalSum)
				l.CustomInfo(msg)

				// Commit new reference value from original value field
				f = fmt.Sprintf("%s_%s", n, referenceConferenceField)
				err = c.PostQuery(e.Target.DestinationMeasurement, n, f, intStatusValue, queryTime)
				l.CustomError(err)
				msg = fmt.Sprintf("[7] Post reference value into '%s' field for %s: %d", f, n, intStatusValue)
				l.CustomInfo(msg)
			}

			refFinal = intFinal
			// Decrease days range
			startingRange--
			endindRange--
		}
	}
}

func TimeRoutine(e Environnement, c Connection, l Log, t Timestamps, ts time.Time) {
	todayField := "today_beginning_date"
	weekField := "week_beginning_date"
	monthField := "month_beginning_date"
	yearField := "year_beginning_date"

	hostname, err := os.Hostname()
	l.CustomError(err)

	// Post today beginning date
	err = c.PostQuery(e.Target.DestinationMeasurement, hostname, todayField, int(t.TodayBeginningDate), ts)
	l.CustomError(err)
	msg := fmt.Sprintf("[1] Post today beginning date value into '%s' fields : %d", todayField, t.TodayBeginningDate)
	l.CustomInfo(msg)

	// Post week beginning date
	err = c.PostQuery(e.Target.DestinationMeasurement, hostname, weekField, int(t.WeekBeginningDate), ts)
	l.CustomError(err)
	msg = fmt.Sprintf("[2] Post week beginning date value into '%s' fields : %d", weekField, t.WeekBeginningDate)
	l.CustomInfo(msg)

	// Post month beginning date
	err = c.PostQuery(e.Target.DestinationMeasurement, hostname, monthField, int(t.MonthBeginningDate), ts)
	l.CustomError(err)
	msg = fmt.Sprintf("[3] Post month beginning date value into '%s' fields : %d", monthField, t.MonthBeginningDate)
	l.CustomInfo(msg)

	// Post year beginning date
	err = c.PostQuery(e.Target.DestinationMeasurement, hostname, yearField, int(t.YearBeginningDate), ts)
	l.CustomError(err)
	msg = fmt.Sprintf("[4] Post month beginning date value into '%s' fields : %d", yearField, t.YearBeginningDate)
	l.CustomInfo(msg)
}
