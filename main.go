package main

import (
	"flag"
	"fmt"
	"time"
)

const (
	configFile string = "/etc/jitsi-stats-collector.toml"
)

var (
	e Environnement = Environnement{}
	c Connection    = Connection{}
	l Log           = Log{}
	t Timestamps    = Timestamps{}
)

func main() {
	// Init custom logger
	l.NewStandardOutput()

	// Load settings
	err := e.NewSettings(configFile)
	l.CustomError(err)

	// Open database connection
	c.InitConnection(e.ReworkInfoDB.Endpoint, e.ReworkInfoDB.Credential, e.InfoDB.Database)
	defer c.Close()

	// Make flags for manage program
	var init = flag.Bool("init", false, "Initialize the history of database since n days.")
	var daemon = flag.Bool("daemon", false, "Run JSC like a daemon.")
	var timestamps = flag.Bool("timestamps", false, "Push time série into database.")
	var initDays = flag.Int("days", 0, "Specify number of days for initialize history.")
	flag.Parse()

	switch {
	case *init:
		if *initDays == 0 {
			msg := fmt.Errorf("missing days values: value=%d. Fill it with '-days' option with value superior than 0", *initDays)
			l.CustomError(msg)
		}

		l.CustomInfo("=== Start to process to the initialization ===")
		for _, fieldName := range e.Target.Fields {
			msg := fmt.Sprintf("=== Start processing for field '%s' ===", fieldName)
			l.CustomInfo(msg)
			InitRoutine(e, c, l, fieldName, initDays)
		}
	case *daemon:
		// Daemonize process
		for {
			l.CustomInfo("=== Start processing for timestamps ===")
			// Timestamps processing
			t.SetTime()
			TimeRoutine(e, c, l, t, time.Now())
			// Jitsi processing
			for _, fieldName := range e.Target.Fields {
				msg := fmt.Sprintf("=== Start processing for field '%s' ===", fieldName)
				l.CustomInfo(msg)
				MainRoutine(e, c, l, fieldName)
			}
			msg := fmt.Sprintf("=== Wait %d secondes for refresh ===", e.Parameters.Refresh)
			l.CustomInfo(msg)
			// Waits for the number of seconds specified in the configuration file
			time.Sleep(time.Duration(e.Parameters.Refresh) * time.Second)
		}
	case *timestamps:
		l.CustomInfo("=== Start processing for timestamps ===")
		// Timestamps processing
		t.SetTime()
		TimeRoutine(e, c, l, t, time.Now())
	}
}
